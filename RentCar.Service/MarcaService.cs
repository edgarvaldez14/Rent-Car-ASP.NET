﻿using System.Collections.Generic;
using RentCar.Data;
using RentCar.Model;

namespace RentCar.Service
{
    public interface IMarcaService : IService<Marca>
    {

    }

    public class MarcaService : AbstractService<Marca>, IMarcaService
    {
        private readonly IMarcaRepository _marcaRepository;

        public MarcaService()
        {
            _marcaRepository = new MarcaRepository(DbFactory);
        }

        public override Marca Get(int id, bool includeInactive = false)
        {
            return _marcaRepository.GetById(id, includeInactive);
        }

        public override IEnumerable<Marca> GetAll(bool includeInactive = false)
        {
            return _marcaRepository.GetAll(includeInactive);
        }

        public override void Create(Marca entity)
        {
            _marcaRepository.Add(entity);
        }

        public override void Delete(Marca entity)
        {
            _marcaRepository.Delete(entity);
        }

        public override void Update(Marca entity)
        {
            _marcaRepository.Update(entity);
        }
    }
}
