﻿using System.Collections.Generic;
using RentCar.Data;
using RentCar.Model;

namespace RentCar.Service
{
    public interface ITandaService : IService<Tanda>
    {

    }

    public class TandaService : AbstractService<Tanda>, ITandaService
    {
        private readonly ITandaRepository _tandaRepository;

        public TandaService()
        {
            _tandaRepository = new TandaRepository(DbFactory);
        }

        public override Tanda Get(int id, bool includeInactive = false)
        {
            return _tandaRepository.GetById(id, includeInactive);
        }

        public override void Create(Tanda entity)
        {
            _tandaRepository.Add(entity);
        }

        public override void Delete(Tanda entity)
        {
            _tandaRepository.Delete(entity);
        }

        public override void Update(Tanda entity)
        {
            _tandaRepository.Update(entity);
        }

        public override IEnumerable<Tanda> GetAll(bool includeInactive = false)
        {
            return _tandaRepository.GetAll(includeInactive);
        }
    }
}
