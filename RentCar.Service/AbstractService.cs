﻿using System;
using System.Collections.Generic;
using RentCar.Data.Infrastructure;
using RentCar.Model;

namespace RentCar.Service
{
    public interface IService<T> where T : AbstractEntity
    {
        IEnumerable<T> GetAll(bool includeInactive = false);
        T Get(int id, bool includeInactive = false);
        void Create(T empleado);
        void Delete(T empleado);
        void Update(T empleado);
        void SaveChanges();
    }

    public abstract class AbstractService<T> : IService<T>, IDisposable where T : AbstractEntity
    {
        protected readonly IDbFactory DbFactory;
        protected readonly IUnitOfWork UnitOfWork;

        protected AbstractService()
        {
            DbFactory = new DbFactory();
            UnitOfWork = new UnitOfWork(DbFactory);
        }

        public abstract T Get(int id, bool includeInactive = false);
        public abstract IEnumerable<T> GetAll(bool includeInactive = false);
        public abstract void Create(T entity);
        public abstract void Delete(T entity);
        public abstract void Update(T entity);


        public void SaveChanges()
        {
            UnitOfWork.Commit();
        }

        public void Dispose()
        {
            DbFactory.Dispose();
        }
    }
}
