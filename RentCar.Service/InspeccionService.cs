﻿using System.Collections.Generic;
using RentCar.Data;
using RentCar.Model;

namespace RentCar.Service
{
    public interface IInspeccionService : IService<Inspeccion>
    {

    }

    public class InspeccionService : AbstractService<Inspeccion>
    {
        private readonly IInspeccionRepository _inspeccionRepository;

        public InspeccionService()
        {
            _inspeccionRepository = new InspeccionRepository(DbFactory);
        }

        public override Inspeccion Get(int id, bool includeInactive = false)
        {
            return _inspeccionRepository.GetById(id, includeInactive);
        }

        public override IEnumerable<Inspeccion> GetAll(bool includeInactive = false)
        {
            return _inspeccionRepository.GetAll(includeInactive);
        }

        public override void Update(Inspeccion entity)
        {
            _inspeccionRepository.Update(entity);
        }

        public override void Create(Inspeccion entity)
        {
            _inspeccionRepository.Add(entity);
        }

        public override void Delete(Inspeccion entity)
        {
            _inspeccionRepository.Delete(entity);
        }
    }
}
