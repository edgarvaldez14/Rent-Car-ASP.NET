﻿using System.Collections.Generic;
using RentCar.Data;
using RentCar.Model;

namespace RentCar.Service
{
    public interface IVehiculoService : IService<Vehiculo>
    {

    }

    public class VehiculoService : AbstractService<Vehiculo>, IVehiculoService
    {
        private readonly IVehiculoRepository _vehiculoRepository;

        public VehiculoService()
        {
            _vehiculoRepository = new VehiculoRepository(DbFactory);
        }

        public override Vehiculo Get(int id, bool includeInactive = false)
        {
            return _vehiculoRepository.GetById(id, includeInactive);
        }

        public override void Create(Vehiculo entity)
        {
            _vehiculoRepository.Add(entity);
        }

        public override void Delete(Vehiculo entity)
        {
            _vehiculoRepository.Delete(entity);
        }

        public override void Update(Vehiculo entity)
        {
            _vehiculoRepository.Update(entity);
        }

        public override IEnumerable<Vehiculo> GetAll(bool includeInactive = false)
        {
            return _vehiculoRepository.GetAll(includeInactive);
        }
    }
}
