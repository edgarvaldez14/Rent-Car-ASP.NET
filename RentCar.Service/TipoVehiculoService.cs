﻿using System.Collections.Generic;
using RentCar.Data;
using RentCar.Model;

namespace RentCar.Service
{
    public interface ITipoVehiculoService : IService<TipoVehiculo>
    {

    }

    public class TipoVehiculoService : AbstractService<TipoVehiculo>, ITipoVehiculoService
    {
        private readonly ITipoVehiculoRepository _tipoVehiculoRepository;

        public TipoVehiculoService()
        {
            _tipoVehiculoRepository = new TipoVehiculoRepository(DbFactory);
        }

        public override TipoVehiculo Get(int id, bool includeInactive = false)
        {
            return _tipoVehiculoRepository.GetById(id, includeInactive);
        }

        public override void Create(TipoVehiculo entity)
        {
            _tipoVehiculoRepository.Add(entity);
        }

        public override void Delete(TipoVehiculo entity)
        {
            _tipoVehiculoRepository.Delete(entity);
        }

        public override void Update(TipoVehiculo entity)
        {
            _tipoVehiculoRepository.Update(entity);
        }

        public override IEnumerable<TipoVehiculo> GetAll(bool includeInactive = false)
        {
            return _tipoVehiculoRepository.GetAll(includeInactive);
        }
    }
}
