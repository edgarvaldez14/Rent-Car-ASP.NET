﻿using System.Collections.Generic;
using RentCar.Data;
using RentCar.Model;

namespace RentCar.Service
{
    public interface IClienteService
    {

    }

    public class ClienteService : AbstractService<Cliente>, IClienteService
    {
        private readonly IClientRepository _clientRepository;

        public ClienteService()
        {
            _clientRepository = new ClienteRepository(DbFactory);
        }

        public override Cliente Get(int id, bool includeInactive = false)
        {
            return _clientRepository.GetById(id, includeInactive);
        }

        public override IEnumerable<Cliente> GetAll(bool includeInactive = false)
        {
            return _clientRepository.GetAll(includeInactive);
        }

        public override void Update(Cliente entity)
        {
            _clientRepository.Update(entity);
        }

        public override void Create(Cliente entity)
        {
            _clientRepository.Add(entity);
        }

        public override void Delete(Cliente entity)
        {
            _clientRepository.Delete(entity);
        }
    }
}
