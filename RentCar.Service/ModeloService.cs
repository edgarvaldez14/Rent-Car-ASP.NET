﻿using System.Collections.Generic;
using RentCar.Data;
using RentCar.Model;

namespace RentCar.Service
{
    public interface IModeloService : IService<Modelo>
    {

    }

    public class ModeloService : AbstractService<Modelo>, IModeloService
    {
        private readonly IModeloRepository _modeloRepository;

        public ModeloService()
        {
            _modeloRepository = new ModeloRepository(DbFactory);
        }

        public override Modelo Get(int id, bool includeInactive = false)
        {
            return _modeloRepository.GetById(id, includeInactive);
        }

        public override void Create(Modelo entity)
        {
            _modeloRepository.Add(entity);
        }

        public override void Delete(Modelo entity)
        {
            _modeloRepository.Delete(entity);
        }

        public override void Update(Modelo entity)
        {
            _modeloRepository.Update(entity);
        }

        public override IEnumerable<Modelo> GetAll(bool includeInactive = false)
        {
            return _modeloRepository.GetAll(includeInactive);
        }
    }
}
