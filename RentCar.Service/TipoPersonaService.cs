﻿using System.Collections.Generic;
using RentCar.Data;
using RentCar.Model;

namespace RentCar.Service
{
    public interface ITipoPersonaService : IService<TipoPersona>
    {

    }

    public class TipoPersonaService : AbstractService<TipoPersona>, ITipoPersonaService
    {
        private readonly ITipoPersonaRepository _tipoPersonaRepository;

        public TipoPersonaService()
        {
            _tipoPersonaRepository = new TipoPersonaRepository(DbFactory);
        }

        public override TipoPersona Get(int id, bool includeInactive = false)
        {
            return _tipoPersonaRepository.GetById(id, includeInactive);
        }

        public override void Create(TipoPersona entity)
        {
            _tipoPersonaRepository.Add(entity);
        }

        public override void Delete(TipoPersona entity)
        {
            _tipoPersonaRepository.Delete(entity);
        }

        public override void Update(TipoPersona entity)
        {
            _tipoPersonaRepository.Update(entity);
        }

        public override IEnumerable<TipoPersona> GetAll(bool includeInactive = false)
        {
            return _tipoPersonaRepository.GetAll(includeInactive);
        }
    }
}
