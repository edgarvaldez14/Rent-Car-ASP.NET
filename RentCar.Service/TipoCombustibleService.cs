﻿using System.Collections.Generic;
using RentCar.Data;
using RentCar.Model;

namespace RentCar.Service
{
    public interface ITipoCombustibleService : IService<TipoCombustible>
    {

    }

    public class TipoCombustibleService : AbstractService<TipoCombustible>, ITipoCombustibleService
    {
        private readonly ITipoCombustibleRepository _tipoCombustibleRepository;

        public TipoCombustibleService()
        {
            _tipoCombustibleRepository = new TipoCombustibleRepository(DbFactory);
        }

        public override TipoCombustible Get(int id, bool includeInactive = false)
        {
            return _tipoCombustibleRepository.GetById(id, includeInactive);
        }

        public override void Create(TipoCombustible entity)
        {
            _tipoCombustibleRepository.Add(entity);
        }

        public override void Delete(TipoCombustible entity)
        {
            _tipoCombustibleRepository.Delete(entity);
        }

        public override void Update(TipoCombustible entity)
        {
            _tipoCombustibleRepository.Update(entity);
        }

        public override IEnumerable<TipoCombustible> GetAll(bool includeInactive = false)
        {
            return _tipoCombustibleRepository.GetAll(includeInactive);
        }
    }
}
