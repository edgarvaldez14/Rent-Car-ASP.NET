﻿using System.Collections.Generic;
using RentCar.Data;
using RentCar.Model;

namespace RentCar.Service
{
    public interface IRentaService : IService<Renta>
    {

    }

    public class RentaService : AbstractService<Renta>, IRentaService
    {
        private readonly IRentaRepository _rentaRepository;

        public RentaService()
        {
            _rentaRepository = new RentaRepository(DbFactory);
        }

        public override Renta Get(int id, bool includeInactive = false)
        {
            return _rentaRepository.GetById(id, includeInactive);
        }

        public override void Create(Renta entity)
        {
            _rentaRepository.Add(entity);
        }

        public override void Delete(Renta entity)
        {
            _rentaRepository.Delete(entity);
        }

        public override void Update(Renta entity)
        {
            _rentaRepository.Update(entity);
        }

        public override IEnumerable<Renta> GetAll(bool includeInactive = false)
        {
            return _rentaRepository.GetAll(includeInactive);
        }
    }
}
