﻿using System.Collections.Generic;
using RentCar.Data;
using RentCar.Model;

namespace RentCar.Service
{
    public interface IEmpleadoService : IService<Empleado>
    {

    }

    public class EmpleadoService : AbstractService<Empleado>, IEmpleadoService
    {
        private readonly IEmpleadoRepository _empleadoRepository;

        public EmpleadoService()
        {
            _empleadoRepository = new EmpleadoRepository(DbFactory);
        }

        public override Empleado Get(int id, bool includeInactive = false)
        {
            return _empleadoRepository.GetById(id, includeInactive);
        }

        public override IEnumerable<Empleado> GetAll(bool includeInactive = false)
        {
            return _empleadoRepository.GetAll(includeInactive);
        }

        public override void Update(Empleado entity)
        {
            _empleadoRepository.Update(entity);
        }

        public override void Create(Empleado entity)
        {
            _empleadoRepository.Add(entity);
        }

        public override void Delete(Empleado entity)
        {
            _empleadoRepository.Delete(entity);
        }
    }
}
