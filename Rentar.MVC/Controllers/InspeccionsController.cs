﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using RentCar.Data;
using RentCar.Model;

namespace Rentar.MVC.Controllers
{
    public class InspeccionsController : Controller
    {
        private RentCarEntities db = new RentCarEntities();

        // GET: Inspeccions
        public ActionResult Index()
        {
            var inspecciones = db.Inspecciones.Include(i => i.Cliente).Include(i => i.Empleado).Include(i => i.Vehiculo);
            return View(inspecciones.ToList());
        }

        // GET: Inspeccions/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Inspeccion inspeccion = db.Inspecciones.Find(id);
            if (inspeccion == null)
            {
                return HttpNotFound();
            }
            return View(inspeccion);
        }

        // GET: Inspeccions/Create
        public ActionResult Create()
        {
            ViewBag.IdCliente = new SelectList(db.Clientes, "IdCliente", "Nombre");
            ViewBag.IdEmpleado = new SelectList(db.Empleados, "IdEmpleado", "Nombre");
            ViewBag.IdVehiculo = new SelectList(db.Vehiculos, "IdVehiculo", "Descripcion");
            return View();
        }

        // POST: Inspeccions/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "IdInspeccion,Ralladuras,CantidadCombustible,GomaRespuesto,Gato,Roturas,GomasEstables,Observaciones,Fecha,IdVehiculo,IdCliente,IdEmpleado,Estado")] Inspeccion inspeccion)
        {
            if (ModelState.IsValid)
            {
                db.Inspecciones.Add(inspeccion);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.IdCliente = new SelectList(db.Clientes, "IdCliente", "Nombre", inspeccion.IdCliente);
            ViewBag.IdEmpleado = new SelectList(db.Empleados, "IdEmpleado", "Nombre", inspeccion.IdEmpleado);
            ViewBag.IdVehiculo = new SelectList(db.Vehiculos, "IdVehiculo", "Descripcion", inspeccion.IdVehiculo);
            return View(inspeccion);
        }

        // GET: Inspeccions/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Inspeccion inspeccion = db.Inspecciones.Find(id);
            if (inspeccion == null)
            {
                return HttpNotFound();
            }
            ViewBag.IdCliente = new SelectList(db.Clientes, "IdCliente", "Nombre", inspeccion.IdCliente);
            ViewBag.IdEmpleado = new SelectList(db.Empleados, "IdEmpleado", "Nombre", inspeccion.IdEmpleado);
            ViewBag.IdVehiculo = new SelectList(db.Vehiculos, "IdVehiculo", "Descripcion", inspeccion.IdVehiculo);
            return View(inspeccion);
        }

        // POST: Inspeccions/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "IdInspeccion,Ralladuras,CantidadCombustible,GomaRespuesto,Gato,Roturas,GomasEstables,Observaciones,Fecha,IdVehiculo,IdCliente,IdEmpleado,Estado")] Inspeccion inspeccion)
        {
            if (ModelState.IsValid)
            {
                db.Entry(inspeccion).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.IdCliente = new SelectList(db.Clientes, "IdCliente", "Nombre", inspeccion.IdCliente);
            ViewBag.IdEmpleado = new SelectList(db.Empleados, "IdEmpleado", "Nombre", inspeccion.IdEmpleado);
            ViewBag.IdVehiculo = new SelectList(db.Vehiculos, "IdVehiculo", "Descripcion", inspeccion.IdVehiculo);
            return View(inspeccion);
        }

        // GET: Inspeccions/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Inspeccion inspeccion = db.Inspecciones.Find(id);
            if (inspeccion == null)
            {
                return HttpNotFound();
            }
            return View(inspeccion);
        }

        // POST: Inspeccions/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Inspeccion inspeccion = db.Inspecciones.Find(id);
            db.Inspecciones.Remove(inspeccion);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
