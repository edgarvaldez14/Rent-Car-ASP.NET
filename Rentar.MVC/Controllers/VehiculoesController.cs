﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using RentCar.Data;
using RentCar.Model;

namespace Rentar.MVC.Controllers
{
    public class VehiculoesController : Controller
    {
        private RentCarEntities db = new RentCarEntities();

        // GET: Vehiculoes
        public ActionResult Index()
        {
            var vehiculos = db.Vehiculos.Include(v => v.Modelo).Include(v => v.TipoCombustible).Include(v => v.TipoVehiculo);
            return View(vehiculos.ToList());
        }

        // GET: Vehiculoes/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Vehiculo vehiculo = db.Vehiculos.Find(id);
            if (vehiculo == null)
            {
                return HttpNotFound();
            }
            return View(vehiculo);
        }

        // GET: Vehiculoes/Create
        public ActionResult Create()
        {
            ViewBag.IdModelo = new SelectList(db.Modelos, "IdModelo", "Descripcion");
            ViewBag.IdTipoCombustible = new SelectList(db.TipoCombustibles, "IdTipoCombustible", "Descripcion");
            ViewBag.IdTipoVehiculo = new SelectList(db.TipoVehiculos, "IdTipoVehiculo", "Descripcion");
            return View();
        }

        // POST: Vehiculoes/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "IdVehiculo,Descripcion,NoChasis,NoMotor,NoPlaca,IdTipoVehiculo,IdModelo,IdTipoCombustible,Estado")] Vehiculo vehiculo)
        {
            if (ModelState.IsValid)
            {
                db.Vehiculos.Add(vehiculo);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.IdModelo = new SelectList(db.Modelos, "IdModelo", "Descripcion", vehiculo.IdModelo);
            ViewBag.IdTipoCombustible = new SelectList(db.TipoCombustibles, "IdTipoCombustible", "Descripcion", vehiculo.IdTipoCombustible);
            ViewBag.IdTipoVehiculo = new SelectList(db.TipoVehiculos, "IdTipoVehiculo", "Descripcion", vehiculo.IdTipoVehiculo);
            return View(vehiculo);
        }

        // GET: Vehiculoes/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Vehiculo vehiculo = db.Vehiculos.Find(id);
            if (vehiculo == null)
            {
                return HttpNotFound();
            }
            ViewBag.IdModelo = new SelectList(db.Modelos, "IdModelo", "Descripcion", vehiculo.IdModelo);
            ViewBag.IdTipoCombustible = new SelectList(db.TipoCombustibles, "IdTipoCombustible", "Descripcion", vehiculo.IdTipoCombustible);
            ViewBag.IdTipoVehiculo = new SelectList(db.TipoVehiculos, "IdTipoVehiculo", "Descripcion", vehiculo.IdTipoVehiculo);
            return View(vehiculo);
        }

        // POST: Vehiculoes/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "IdVehiculo,Descripcion,NoChasis,NoMotor,NoPlaca,IdTipoVehiculo,IdModelo,IdTipoCombustible,Estado")] Vehiculo vehiculo)
        {
            if (ModelState.IsValid)
            {
                db.Entry(vehiculo).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.IdModelo = new SelectList(db.Modelos, "IdModelo", "Descripcion", vehiculo.IdModelo);
            ViewBag.IdTipoCombustible = new SelectList(db.TipoCombustibles, "IdTipoCombustible", "Descripcion", vehiculo.IdTipoCombustible);
            ViewBag.IdTipoVehiculo = new SelectList(db.TipoVehiculos, "IdTipoVehiculo", "Descripcion", vehiculo.IdTipoVehiculo);
            return View(vehiculo);
        }

        // GET: Vehiculoes/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Vehiculo vehiculo = db.Vehiculos.Find(id);
            if (vehiculo == null)
            {
                return HttpNotFound();
            }
            return View(vehiculo);
        }

        // POST: Vehiculoes/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Vehiculo vehiculo = db.Vehiculos.Find(id);
            db.Vehiculos.Remove(vehiculo);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
