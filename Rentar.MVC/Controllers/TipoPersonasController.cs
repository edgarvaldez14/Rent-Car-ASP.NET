﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using RentCar.Data;
using RentCar.Model;

namespace Rentar.MVC.Controllers
{
    public class TipoPersonasController : Controller
    {
        private RentCarEntities db = new RentCarEntities();

        // GET: TipoPersonas
        public ActionResult Index()
        {
            return View(db.TipoPersonas.ToList());
        }

        // GET: TipoPersonas/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TipoPersona tipoPersona = db.TipoPersonas.Find(id);
            if (tipoPersona == null)
            {
                return HttpNotFound();
            }
            return View(tipoPersona);
        }

        // GET: TipoPersonas/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: TipoPersonas/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "IdTipoPersona,Descripcion,Estado")] TipoPersona tipoPersona)
        {
            if (ModelState.IsValid)
            {
                db.TipoPersonas.Add(tipoPersona);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(tipoPersona);
        }

        // GET: TipoPersonas/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TipoPersona tipoPersona = db.TipoPersonas.Find(id);
            if (tipoPersona == null)
            {
                return HttpNotFound();
            }
            return View(tipoPersona);
        }

        // POST: TipoPersonas/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "IdTipoPersona,Descripcion,Estado")] TipoPersona tipoPersona)
        {
            if (ModelState.IsValid)
            {
                db.Entry(tipoPersona).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(tipoPersona);
        }

        // GET: TipoPersonas/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TipoPersona tipoPersona = db.TipoPersonas.Find(id);
            if (tipoPersona == null)
            {
                return HttpNotFound();
            }
            return View(tipoPersona);
        }

        // POST: TipoPersonas/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            TipoPersona tipoPersona = db.TipoPersonas.Find(id);
            db.TipoPersonas.Remove(tipoPersona);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
