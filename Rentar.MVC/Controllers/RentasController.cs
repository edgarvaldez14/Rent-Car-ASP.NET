﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using RentCar.Data;
using RentCar.Model;

namespace Rentar.MVC.Controllers
{
    public class RentasController : Controller
    {
        private RentCarEntities db = new RentCarEntities();

        // GET: Rentas
        public ActionResult Index()
        {
            var rentas = db.Rentas.Include(r => r.Cliente).Include(r => r.Empleado).Include(r => r.Vehiculo);
            return View(rentas.ToList());
        }

        // GET: Rentas/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Renta renta = db.Rentas.Find(id);
            if (renta == null)
            {
                return HttpNotFound();
            }
            return View(renta);
        }

        // GET: Rentas/Create
        public ActionResult Create()
        {
            ViewBag.IdCliente = new SelectList(db.Clientes, "IdCliente", "Nombre");
            ViewBag.IdEmpleado = new SelectList(db.Empleados, "IdEmpleado", "Nombre");
            ViewBag.IdVehiculo = new SelectList(db.Vehiculos, "IdVehiculo", "Descripcion");
            return View();
        }

        // POST: Rentas/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "IdRenta,FechaRenta,FechaDevolucion,MontoDia,CantidadDias,Comentario,IdEmpleado,IdVehiculo,IdCliente,Estado")] Renta renta)
        {
            if (ModelState.IsValid)
            {
                db.Rentas.Add(renta);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.IdCliente = new SelectList(db.Clientes, "IdCliente", "Nombre", renta.IdCliente);
            ViewBag.IdEmpleado = new SelectList(db.Empleados, "IdEmpleado", "Nombre", renta.IdEmpleado);
            ViewBag.IdVehiculo = new SelectList(db.Vehiculos, "IdVehiculo", "Descripcion", renta.IdVehiculo);
            return View(renta);
        }

        // GET: Rentas/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Renta renta = db.Rentas.Find(id);
            if (renta == null)
            {
                return HttpNotFound();
            }
            ViewBag.IdCliente = new SelectList(db.Clientes, "IdCliente", "Nombre", renta.IdCliente);
            ViewBag.IdEmpleado = new SelectList(db.Empleados, "IdEmpleado", "Nombre", renta.IdEmpleado);
            ViewBag.IdVehiculo = new SelectList(db.Vehiculos, "IdVehiculo", "Descripcion", renta.IdVehiculo);
            return View(renta);
        }

        // POST: Rentas/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "IdRenta,FechaRenta,FechaDevolucion,MontoDia,CantidadDias,Comentario,IdEmpleado,IdVehiculo,IdCliente,Estado")] Renta renta)
        {
            if (ModelState.IsValid)
            {
                db.Entry(renta).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.IdCliente = new SelectList(db.Clientes, "IdCliente", "Nombre", renta.IdCliente);
            ViewBag.IdEmpleado = new SelectList(db.Empleados, "IdEmpleado", "Nombre", renta.IdEmpleado);
            ViewBag.IdVehiculo = new SelectList(db.Vehiculos, "IdVehiculo", "Descripcion", renta.IdVehiculo);
            return View(renta);
        }

        // GET: Rentas/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Renta renta = db.Rentas.Find(id);
            if (renta == null)
            {
                return HttpNotFound();
            }
            return View(renta);
        }

        // POST: Rentas/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Renta renta = db.Rentas.Find(id);
            db.Rentas.Remove(renta);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
