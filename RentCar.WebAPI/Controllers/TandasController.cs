﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using RentCar.Data;
using RentCar.Model;

namespace RentCar.WebAPI.Controllers
{
    public class TandasController : ApiController
    {
        RentCarEntities db = new RentCarEntities();
        [HttpGet]
        public IQueryable<Renta> Get()
        {
            return db.Rentas;
        }
        public IHttpActionResult GetRenta(int id)
        {
            Renta renta = db.Rentas.Find(id);
            if (renta == null)
            {
                return NotFound();
            }
            return Ok(renta);
        }
        [HttpPost]
        [ResponseType(typeof(Renta))]
        public IHttpActionResult Post([FromBody] Renta renta)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.Rentas.Add(renta);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = renta.IdRenta }, renta);
        }
    }
}
