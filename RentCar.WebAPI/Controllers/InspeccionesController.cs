﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using RentCar.Data;
using RentCar.Model;

namespace RentCar.WebAPI.Controllers
{
    public class InspeccionesController : ApiController
    {
        RentCarEntities db = new RentCarEntities();
        [HttpGet]
        public IQueryable<Inspeccion> Get()
        {
            return db.Inspecciones;
        }
        public IHttpActionResult GetInspeccion(int id)
        {
            Inspeccion inspeccion = db.Inspecciones.Find(id);
            if (inspeccion == null)
            {
                return NotFound();
            }
            return Ok(inspeccion);
        }
        [HttpPost]
        [ResponseType(typeof(Inspeccion))]
        public IHttpActionResult Post([FromBody] Inspeccion inspeccion)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.Inspecciones.Add(inspeccion);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = inspeccion.IdInspeccion }, inspeccion);
        }
    }
}
