﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using RentCar.Data;
using RentCar.Model;

namespace RentCar.WebAPI.Controllers
{
    public class TipoPersonasController : ApiController
    {
        RentCarEntities db = new RentCarEntities();
        [HttpGet]
        public IQueryable<TipoPersona> Get()
        {
            return db.TipoPersonas;
        }
        public IHttpActionResult GetTipoPersona(int id)
        {
            TipoPersona tipoPersona = db.TipoPersonas.Find(id);
            if (tipoPersona == null)
            {
                return NotFound();
            }
            return Ok(tipoPersona);
        }
        [HttpPost]
        [ResponseType(typeof(TipoPersona))]
        public IHttpActionResult Post([FromBody] TipoPersona tipoPersona)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.TipoPersonas.Add(tipoPersona);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = tipoPersona.IdTipoPersona }, tipoPersona);
        }
    }
}
