﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using RentCar.Data;
using RentCar.Model;

namespace RentCar.WebAPI.Controllers
{
    public class TipoCombustiblesController : ApiController
    {

        RentCarEntities db = new RentCarEntities();
        [HttpGet]
        public IQueryable<TipoCombustible> Get()
        {
            return db.TipoCombustibles;
        }
        public IHttpActionResult GetTipoCombustible(int id)
        {
            TipoCombustible tipoCombustible = db.TipoCombustibles.Find(id);
            if (tipoCombustible == null)
            {
                return NotFound();
            }
            return Ok(tipoCombustible);
        }
        [HttpPost]
        [ResponseType(typeof(TipoCombustible))]
        public IHttpActionResult Post([FromBody] TipoCombustible tipoCombustible)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.TipoCombustibles.Add(tipoCombustible);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = tipoCombustible.IdTipoCombustible }, tipoCombustible);
        }
    }
}
