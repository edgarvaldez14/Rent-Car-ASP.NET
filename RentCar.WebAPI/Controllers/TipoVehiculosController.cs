﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using RentCar.Model;
using RentCar.Data;
using System.Web.Http.Description;

namespace RentCar.WebAPI.Controllers
{
    public class TipoVehiculosController : ApiController
    {
        RentCarEntities db = new RentCarEntities();
        [HttpGet]
        public IQueryable<TipoVehiculo> Get()
        {
            return db.TipoVehiculos;
        }
        public IHttpActionResult GetTipoVehiculo(int id)
        {
            TipoVehiculo tipoVehiculo = db.TipoVehiculos.Find(id);
            if (tipoVehiculo == null)
            {
                return NotFound();
            }
            return Ok(tipoVehiculo);
        }
        [HttpPost]
        [ResponseType(typeof(TipoVehiculo))]
        public IHttpActionResult Post([FromBody] TipoVehiculo tipoVehiculo)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.TipoVehiculos.Add(tipoVehiculo);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = tipoVehiculo.IdTipoVehiculo }, tipoVehiculo);
        }
    }
}
