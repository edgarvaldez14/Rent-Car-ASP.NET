﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using RentCar.Data;
using RentCar.Model;

namespace RentCar.WebAPI.Controllers
{
    public class ModelosController : ApiController
    {
        RentCarEntities db = new RentCarEntities();
        [HttpGet]
        public IQueryable<Modelo> Get()
        {
            return db.Modelos;
        }
        public IHttpActionResult GetModelo(int id)
        {
            Modelo modelo = db.Modelos.Find(id);
            if (modelo == null)
            {
                return NotFound();
            }
            return Ok(modelo);
        }
        [HttpPost]
        [ResponseType(typeof(TipoVehiculo))]
        public IHttpActionResult Post([FromBody] Modelo modelo)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.Modelos.Add(modelo);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = modelo.IdModelo }, modelo);
        }
    }
}
