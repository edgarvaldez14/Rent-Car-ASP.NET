﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using RentCar.Model;
using RentCar.Data;
using System.Web.Http.Description;

namespace RentCar.WebAPI.Controllers
{
    public class EmpleadosController : ApiController
    {
        RentCarEntities db = new RentCarEntities();
        [HttpGet]
        public IQueryable<Empleado> Get()
        {
            return db.Empleados;
        }
        public IHttpActionResult GetEmpleado(int id)
        {
            Empleado empleado = db.Empleados.Find(id);
            if (empleado == null)
            {
                return NotFound();
            }
            return Ok(empleado);
        }
        [HttpPost]
        [ResponseType(typeof(Empleado))]
        public IHttpActionResult Post([FromBody] Empleado empleado)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.Empleados.Add(empleado);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = empleado.IdEmpleado }, empleado);
        }
    }
}
