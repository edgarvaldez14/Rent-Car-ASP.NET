﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using RentCar.Model;
using RentCar.Data;
using System.Web.Http.Description;

namespace RentCar.WebAPI.Controllers
{
    public class ClientesController : ApiController
    {
        RentCarEntities db = new RentCarEntities();
        [HttpGet]
        public IQueryable<Cliente> Get()
        {
            return db.Clientes;
        }
        public IHttpActionResult GetCliente(int id)
        {
            Cliente cliente = db.Clientes.Find(id);
            if (cliente == null)
            {
                return NotFound();
            }
            return Ok(cliente);
        }
        [HttpPost]
        [ResponseType(typeof(Cliente))]
        public IHttpActionResult Post(Cliente cliente)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.Clientes.Add(cliente);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = cliente.IdCliente }, cliente);
        }
    }
}
