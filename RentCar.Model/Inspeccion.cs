﻿using System;

namespace RentCar.Model
{
    public class Inspeccion : AbstractEntity
    {
        public int IdInspeccion { get; set; }
        public bool Ralladuras { get; set; }
        public double CantidadCombustible { get; set; }
        public bool GomaRespuesto { get; set; }
        public bool Gato { get; set; }
        public bool Roturas { get; set; }
        public int GomasEstables { get; set; }
        public string Observaciones { get; set; }
        public DateTime Fecha { get; set; }

        public int IdVehiculo { get; set; }
        public Vehiculo Vehiculo { get; set; }

        public int IdCliente { get; set; }
        public Cliente Cliente { get; set; }

        public int IdEmpleado { get; set; }
        public Empleado Empleado { get; set; }
    }
}
