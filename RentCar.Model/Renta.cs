﻿using System;

namespace RentCar.Model
{
    public class Renta : AbstractEntity
    {
        public int IdRenta { get; set; }
        public DateTime FechaRenta { get; set; }
        public DateTime? FechaDevolucion { get; set; }
        public decimal MontoDia { get; set; }
        public int CantidadDias { get; set; }
        public string Comentario { get; set; }

        public int IdEmpleado { get; set; }
        public Empleado Empleado { get; set; }

        public int IdVehiculo { get; set; }
        public Vehiculo Vehiculo { get; set; }

        public int IdCliente { get; set; }
        public Cliente Cliente { get; set; }
    }
}
