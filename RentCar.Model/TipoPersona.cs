﻿using System.Collections.Generic;

namespace RentCar.Model
{
    public class TipoPersona : AbstractEntity
    {
        public int IdTipoPersona { get; set; }
        public string Descripcion { get; set; }

        public List<Cliente> Clientes { get; set; }
    }
}
