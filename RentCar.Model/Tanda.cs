﻿using System.Collections.Generic;

namespace RentCar.Model
{
    public class Tanda : AbstractEntity
    {
        public int IdTanda { get; set; }
        public string Descripcion { get; set; }

        public List<Empleado> Empleados { get; set; }
    }
}
