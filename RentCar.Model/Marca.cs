﻿using System.Collections.Generic;

namespace RentCar.Model
{
    public class Marca : AbstractEntity
    {
        public int IdMarca { get; set; }
        public string Descripcion { get; set; }

        public List<Modelo> Modelos { get; set; }
    }
}
