﻿using System.Collections.Generic;

namespace RentCar.Model
{
    public class Modelo : AbstractEntity
    {
        public int IdModelo { get; set; }
        public string Descripcion { get; set; }

        public int IdMarca { get; set; }
        public Marca Marca { get; set; }

        public List<Vehiculo> Vehiculos { get; set; }
    }
}
