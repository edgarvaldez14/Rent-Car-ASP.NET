﻿using System.Collections.Generic;

namespace RentCar.Model
{
    public class Cliente : AbstractEntity
    {
        public int IdCliente { get; set; }
        public string Nombre { get; set; }
        public string Cedula { get; set; }
        public string NoTarjeta { get; set; }
        public decimal LimiteCredito { get; set; }

        public int IdTipoPersona { get; set; }
        public TipoPersona TipoPersona { get; set; }

        public List<Inspeccion> Inspecciones { get; set; }
        public List<Renta> Rentas { get; set; }
    }
}
