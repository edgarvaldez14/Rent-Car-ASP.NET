﻿using System.Collections.Generic;

namespace RentCar.Model
{
    public class TipoCombustible : AbstractEntity
    {
        public int IdTipoCombustible { get; set; }
        public string Descripcion { get; set; }

        public List<Vehiculo> Vehiculos { get; set; }
    }
}
