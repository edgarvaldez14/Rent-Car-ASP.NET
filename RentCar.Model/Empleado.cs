﻿using System;
using System.Collections.Generic;

namespace RentCar.Model
{
    public class Empleado : AbstractEntity
    {
        public int IdEmpleado { get; set; }
        public string Nombre { get; set; }
        public string Cedula { get; set; }
        public decimal Comision { get; set; }
        public DateTime FechaIngreso { get; set; }

        public int IdTanda { get; set; }
        public Tanda Tanda { get; set; }

        public bool Estado { get; set; }

        public List<Renta> Rentas { get; set; }
        public List<Inspeccion> Inspecciones { get; set; }
    }
}
