﻿using System.Collections.Generic;

namespace RentCar.Model
{
    public class TipoVehiculo : AbstractEntity
    {
        public int IdTipoVehiculo { get; set; }
        public string Descripcion { get; set; }

        public List<Vehiculo> Vehiculos { get; set; }
    }
}
