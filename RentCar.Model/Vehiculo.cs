﻿using System.Collections.Generic;

namespace RentCar.Model
{
    public class Vehiculo : AbstractEntity
    {
        public int IdVehiculo { get; set; }
        public string Descripcion { get; set; }
        public string NoChasis { get; set; }
        public string NoMotor { get; set; }
        public string NoPlaca { get; set; }

        public int IdTipoVehiculo { get; set; }
        public TipoVehiculo TipoVehiculo { get; set; }

        public int IdModelo { get; set; }
        public Modelo Modelo { get; set; }
        
        public int IdTipoCombustible { get; set; }
        public TipoCombustible TipoCombustible { get; set; }

        public List<Renta> Rentas { get; set; }
        public List<Inspeccion> Inspecciones { get; set; }
    }
}
