namespace RentCar.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class pepele : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.TipoCombustible", "Descripcion", c => c.String(nullable: false, maxLength: 20));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.TipoCombustible", "Descripcion", c => c.String(nullable: false, maxLength: 10));
        }
    }
}
