// <auto-generated />
namespace RentCar.Data.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.1.3-40302")]
    public sealed partial class rent : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(rent));
        
        string IMigrationMetadata.Id
        {
            get { return "201807111905591_rent"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
