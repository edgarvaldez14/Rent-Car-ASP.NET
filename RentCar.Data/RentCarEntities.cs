﻿using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using RentCar.Data.Configuration;
using RentCar.Model;

namespace RentCar.Data
{
    public class RentCarEntities : DbContext
    {
        public DbSet<Cliente> Clientes { get; set; }
        public DbSet<Empleado> Empleados { get; set; }
        public DbSet<Inspeccion> Inspecciones { get; set; }
        public DbSet<Marca> Marcas { get; set; }
        public DbSet<Modelo> Modelos { get; set; }
        public DbSet<Renta> Rentas { get; set; }
        public DbSet<Tanda> Tandas { get; set; }
        public DbSet<TipoCombustible> TipoCombustibles { get; set; }
        public DbSet<TipoPersona> TipoPersonas { get; set; }
        public DbSet<TipoVehiculo> TipoVehiculos { get; set; }
        public DbSet<Vehiculo> Vehiculos { get; set; }

        public RentCarEntities() : base("RentCarEntities") {}

        public virtual void Commit()
        {
            SaveChanges();
        }
     
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Configurations.Add(new ClienteConfiguration());
            modelBuilder.Configurations.Add(new EmpleadoConfiguration());
            modelBuilder.Configurations.Add(new InspeccionConfiguration());
            modelBuilder.Configurations.Add(new MarcaConfiguration());
            modelBuilder.Configurations.Add(new ModeloConfiguration());
            modelBuilder.Configurations.Add(new RentaConfiguration());
            modelBuilder.Configurations.Add(new TandaConfiguration());
            modelBuilder.Configurations.Add(new TipoCombustibleConfiguration());
            modelBuilder.Configurations.Add(new TipoPersonaConfiguration());
            modelBuilder.Configurations.Add(new TipoVehiculoConfiguration());
            modelBuilder.Configurations.Add(new VehiculoConfiguration());


        }
    }
}
