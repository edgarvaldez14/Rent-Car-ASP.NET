﻿using RentCar.Data.Infrastructure;
using RentCar.Model;

namespace RentCar.Data
{
    public interface IModeloRepository : IRepository<Modelo>
    {

    }
    public class ModeloRepository : RepositoryBase<Modelo>, IModeloRepository
    {
        public ModeloRepository(IDbFactory dbFactory) : base(dbFactory)
        {
        }
    }
}