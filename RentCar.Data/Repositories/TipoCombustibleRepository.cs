﻿using RentCar.Data.Infrastructure;
using RentCar.Model;

namespace RentCar.Data
{
    public interface ITipoCombustibleRepository : IRepository<TipoCombustible>
    {

    }
    public class TipoCombustibleRepository : RepositoryBase<TipoCombustible>, ITipoCombustibleRepository
    {
        public TipoCombustibleRepository(IDbFactory dbFactory) : base(dbFactory)
        {
        }
    }
}