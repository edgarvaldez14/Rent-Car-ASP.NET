﻿using RentCar.Data.Infrastructure;
using RentCar.Model;

namespace RentCar.Data
{
    public interface IEmpleadoRepository : IRepository<Empleado>
    {

    }
    public class EmpleadoRepository : RepositoryBase<Empleado>, IEmpleadoRepository
    {
        public EmpleadoRepository(IDbFactory dbFactory) : base(dbFactory)
        {
        }
    }
}
