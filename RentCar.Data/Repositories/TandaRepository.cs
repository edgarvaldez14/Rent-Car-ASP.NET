﻿using RentCar.Data.Infrastructure;
using RentCar.Model;

namespace RentCar.Data
{
    public interface ITandaRepository : IRepository<Tanda>
    {

    }
    public class TandaRepository : RepositoryBase<Tanda>, ITandaRepository
    {
        public TandaRepository(IDbFactory dbFactory) : base(dbFactory)
        {
        }
    }
}