﻿using RentCar.Data.Infrastructure;
using RentCar.Model;

namespace RentCar.Data
{
    public interface IInspeccionRepository : IRepository<Inspeccion>
    {

    }
    public class InspeccionRepository : RepositoryBase<Inspeccion>, IInspeccionRepository
    {
        public InspeccionRepository(IDbFactory dbFactory) : base(dbFactory)
        {
        }
    }
}
