﻿using RentCar.Data.Infrastructure;
using RentCar.Model;

namespace RentCar.Data
{
    public interface IRentaRepository : IRepository<Renta>
    {

    }
    public class RentaRepository : RepositoryBase<Renta>, IRentaRepository
    {
        public RentaRepository(IDbFactory dbFactory) : base(dbFactory)
        {
        }
    }
}