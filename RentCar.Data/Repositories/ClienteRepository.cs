﻿using RentCar.Data.Infrastructure;
using RentCar.Model;

namespace RentCar.Data
{
    public interface IClientRepository : IRepository<Cliente>
    {

    }
    public class ClienteRepository : RepositoryBase<Cliente>, IClientRepository
    {
        public ClienteRepository(IDbFactory dbFactory) : base(dbFactory)
        {
        }
    }
}
