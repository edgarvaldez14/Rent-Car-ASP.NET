﻿using RentCar.Data.Infrastructure;
using RentCar.Model;

namespace RentCar.Data
{
    public interface IVehiculoRepository : IRepository<Vehiculo>
    {

    }
    public class VehiculoRepository : RepositoryBase<Vehiculo>, IVehiculoRepository
    {
        public VehiculoRepository(IDbFactory dbFactory) : base(dbFactory)
        {
        }
    }
}