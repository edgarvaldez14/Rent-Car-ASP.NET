﻿using RentCar.Data.Infrastructure;
using RentCar.Model;

namespace RentCar.Data
{
    public interface IMarcaRepository : IRepository<Marca>
    {

    }
    public class MarcaRepository : RepositoryBase<Marca>, IMarcaRepository
    {
        public MarcaRepository(IDbFactory dbFactory) : base(dbFactory)
        {
        }
    }
}