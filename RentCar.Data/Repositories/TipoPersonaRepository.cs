﻿using RentCar.Data.Infrastructure;
using RentCar.Model;

namespace RentCar.Data
{
    public interface ITipoPersonaRepository : IRepository<TipoPersona>
    {

    }
    public class TipoPersonaRepository : RepositoryBase<TipoPersona>, ITipoPersonaRepository
    {
        public TipoPersonaRepository(IDbFactory dbFactory) : base(dbFactory)
        {
        }
    }
}