﻿using RentCar.Data.Infrastructure;
using RentCar.Model;

namespace RentCar.Data
{
    public interface ITipoVehiculoRepository : IRepository<TipoVehiculo>
    {

    }
    public class TipoVehiculoRepository : RepositoryBase<TipoVehiculo>, ITipoVehiculoRepository
    {
        public TipoVehiculoRepository(IDbFactory dbFactory) : base(dbFactory)
        {
        }
    }
}