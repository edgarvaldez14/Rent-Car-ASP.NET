﻿using System.Data.Entity.ModelConfiguration;
using RentCar.Model;

namespace RentCar.Data.Configuration
{
    public class MarcaConfiguration : EntityTypeConfiguration<Marca>
    {
        public MarcaConfiguration()
        {
            ToTable("Marca");
            HasKey(m => m.IdMarca);
            Property(m => m.Descripcion).IsRequired().HasMaxLength(20);
            Property(m => m.Estado).IsRequired();
        }
    }
}
