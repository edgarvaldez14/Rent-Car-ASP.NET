﻿using System.Data.Entity.ModelConfiguration;
using RentCar.Model;

namespace RentCar.Data.Configuration
{
    public class InspeccionConfiguration : EntityTypeConfiguration<Inspeccion>
    {
        public InspeccionConfiguration()
        {
            ToTable("Inspeccion");
            HasKey(m => m.IdInspeccion);
            Property(m => m.Ralladuras).IsRequired();
            Property(m => m.CantidadCombustible).IsRequired();
            Property(m => m.GomaRespuesto).IsRequired();
            Property(m => m.Gato).IsRequired();
            Property(m => m.Roturas).IsRequired();
            Property(m => m.GomasEstables).IsRequired();
            Property(m => m.Observaciones).IsRequired().HasMaxLength(500);
            Property(m => m.Fecha).IsRequired();

            Property(m => m.IdVehiculo).IsRequired();
            HasRequired(m => m.Vehiculo).WithMany(n => n.Inspecciones).HasForeignKey(m => m.IdVehiculo);

            Property(m => m.IdCliente).IsRequired();
            HasRequired(m => m.Cliente).WithMany(n => n.Inspecciones).HasForeignKey(m => m.IdCliente);

            Property(m => m.IdEmpleado).IsRequired();
            HasRequired(m => m.Empleado).WithMany(n => n.Inspecciones).HasForeignKey(m => m.IdEmpleado);

            Property(m => m.Estado).IsRequired();
        }
    }
}
