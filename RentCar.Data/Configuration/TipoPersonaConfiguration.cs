﻿using System.Data.Entity.ModelConfiguration;
using RentCar.Model;

namespace RentCar.Data.Configuration
{
    public class TipoPersonaConfiguration : EntityTypeConfiguration<TipoPersona>
    {
        public TipoPersonaConfiguration()
        {
            ToTable("TipoPersona");
            HasKey(m => m.IdTipoPersona);
            Property(m => m.Descripcion).IsRequired().HasMaxLength(10);
            Property(m => m.Estado).IsRequired();
        }
    }
}
