﻿using System.Data.Entity.ModelConfiguration;
using RentCar.Model;

namespace RentCar.Data.Configuration
{
    public class ModeloConfiguration : EntityTypeConfiguration<Modelo>
    {
        public ModeloConfiguration()
        {
            ToTable("Modelo");
            HasKey(m => m.IdModelo);
            Property(m => m.Descripcion).IsRequired().HasMaxLength(30);

            Property(m => m.IdMarca).IsRequired();
            HasRequired(m => m.Marca).WithMany(n => n.Modelos).HasForeignKey(m => m.IdMarca);

            Property(m => m.Estado).IsRequired();
        }
    }
}
