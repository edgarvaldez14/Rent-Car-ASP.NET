﻿using System.Data.Entity.ModelConfiguration;
using RentCar.Model;

namespace RentCar.Data.Configuration
{
    public class TandaConfiguration : EntityTypeConfiguration<Tanda>
    {
        public TandaConfiguration()
        {
            ToTable("Tanda");
            HasKey(m => m.IdTanda);
            Property(m => m.Descripcion).IsRequired().HasMaxLength(10);
            Property(m => m.Estado).IsRequired();
        }
    }
}
