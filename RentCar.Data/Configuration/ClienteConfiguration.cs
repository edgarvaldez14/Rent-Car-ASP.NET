﻿using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.Infrastructure.Annotations;
using System.Data.Entity.ModelConfiguration;
using RentCar.Model;

namespace RentCar.Data.Configuration
{
    public class ClienteConfiguration : EntityTypeConfiguration<Cliente>
    {
        public ClienteConfiguration()
        {
            ToTable("Cliente");
            HasKey(m => m.IdCliente);
            Property(m => m.Nombre).IsRequired().HasMaxLength(30).HasColumnAnnotation("Index", new IndexAnnotation(new[] { new IndexAttribute("Index") { IsUnique = true } }));
            Property(m => m.Cedula).IsRequired().HasMaxLength(11).IsFixedLength();
            Property(m => m.NoTarjeta).IsRequired().HasMaxLength(16).IsFixedLength();
            Property(m => m.LimiteCredito).IsRequired().HasPrecision(18, 2);
            
            Property(m => m.IdTipoPersona).IsRequired();
            HasRequired(m => m.TipoPersona).WithMany(n => n.Clientes).HasForeignKey(m => m.IdTipoPersona);

            Property(m => m.Estado).IsRequired();
        }
    }
}
