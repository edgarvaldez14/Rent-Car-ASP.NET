﻿using System.Data.Entity.ModelConfiguration;
using RentCar.Model;

namespace RentCar.Data.Configuration
{
    public class TipoCombustibleConfiguration : EntityTypeConfiguration<TipoCombustible>
    {
        public TipoCombustibleConfiguration()
        {
            ToTable("TipoCombustible");
            HasKey(m => m.IdTipoCombustible);
            Property(m => m.Descripcion).IsRequired().HasMaxLength(20);
            Property(m => m.Estado).IsRequired();
        }
    }
}
