﻿using System.Data.Entity.ModelConfiguration;
using RentCar.Model;

namespace RentCar.Data.Configuration
{
    public class EmpleadoConfiguration : EntityTypeConfiguration<Empleado>
    {
        public EmpleadoConfiguration()
        {
            ToTable("Empleado");
            HasKey(m => m.IdEmpleado);
            Property(m => m.Nombre).IsRequired().HasMaxLength(30);
            Property(m => m.Cedula).IsRequired().HasMaxLength(11).IsFixedLength();
            Property(m => m.Comision).IsRequired().HasPrecision(18, 2);
            Property(m => m.FechaIngreso).IsRequired();

            Property(m => m.IdTanda).IsRequired();
            HasRequired(m => m.Tanda).WithMany(n => n.Empleados).HasForeignKey(m => m.IdTanda);
            
            Property(m => m.Estado).IsRequired();
        }
    }
}
