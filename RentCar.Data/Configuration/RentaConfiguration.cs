﻿using System.Data.Entity.ModelConfiguration;
using RentCar.Model;

namespace RentCar.Data.Configuration
{
    public class RentaConfiguration: EntityTypeConfiguration<Renta>
    {
        public RentaConfiguration()
        {
            ToTable("Renta");
            HasKey(m => m.IdRenta);
            Property(m => m.FechaRenta).IsRequired();
            Property(m => m.FechaDevolucion).IsOptional();
            Property(m => m.MontoDia).IsRequired().HasPrecision(18, 2);
            Property(m => m.CantidadDias).IsRequired();
            Property(m => m.Comentario).IsRequired().HasMaxLength(100);

            Property(m => m.IdEmpleado).IsRequired();
            HasRequired(m => m.Empleado).WithMany(n => n.Rentas).HasForeignKey(m => m.IdEmpleado);

            Property(m => m.IdVehiculo).IsRequired();
            HasRequired(m => m.Vehiculo).WithMany(n => n.Rentas).HasForeignKey(m => m.IdVehiculo);

            Property(m => m.IdCliente).IsRequired();
            HasRequired(m => m.Cliente).WithMany(n => n.Rentas).HasForeignKey(m => m.IdCliente);

            Property(m => m.Estado).IsRequired();
        }
    }
}
