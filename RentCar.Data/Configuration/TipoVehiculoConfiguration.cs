﻿using System.Data.Entity.ModelConfiguration;
using RentCar.Model;

namespace RentCar.Data.Configuration
{
    public class TipoVehiculoConfiguration : EntityTypeConfiguration<TipoVehiculo>
    {
        public TipoVehiculoConfiguration()
        {
            ToTable("TipoVehiculo");
            HasKey(m => m.IdTipoVehiculo);
            Property(m => m.Descripcion).IsRequired().HasMaxLength(10);
            Property(m => m.Estado).IsRequired();
        }
    }
}
