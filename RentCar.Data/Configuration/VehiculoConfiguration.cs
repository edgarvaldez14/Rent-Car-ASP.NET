﻿using System.Data.Entity.ModelConfiguration;
using RentCar.Model;

namespace RentCar.Data.Configuration
{
    public class VehiculoConfiguration : EntityTypeConfiguration<Vehiculo>
    {
        public VehiculoConfiguration()
        {
            ToTable("Vehiculo");
            HasKey(m => m.IdVehiculo);
            Property(m => m.Descripcion).IsRequired().HasMaxLength(30);
            Property(m => m.NoChasis).IsRequired().HasMaxLength(30);
            Property(m => m.NoMotor).IsRequired().HasMaxLength(30);
            Property(m => m.NoPlaca).IsRequired().HasMaxLength(30);

            Property(m => m.IdTipoVehiculo).IsRequired();
            HasRequired(m => m.TipoVehiculo).WithMany(n => n.Vehiculos).HasForeignKey(m => m.IdTipoVehiculo);

            Property(m => m.IdModelo).IsRequired();
            HasRequired(m => m.Modelo).WithMany(n => n.Vehiculos).HasForeignKey(m => m.IdModelo);

            Property(m => m.IdTipoCombustible).IsRequired();
            HasRequired(m => m.TipoCombustible).WithMany(n => n.Vehiculos).HasForeignKey(m => m.IdTipoCombustible);

            Property(m => m.Estado).IsRequired();
        }
    }
}
