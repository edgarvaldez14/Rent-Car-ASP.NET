﻿using System.Collections.Generic;
using System.Data.Entity;
using RentCar.Model;

namespace RentCar.Data
{
    public class RentCarSeed : DropCreateDatabaseIfModelChanges<RentCarEntities>
    {
        protected override void Seed(RentCarEntities context)
        {
            context.TipoPersonas.AddRange(GetTipoPersonas());
            context.Tandas.AddRange(GetTandas());
            context.TipoVehiculos.AddRange(GetTipoVehiculos());
            context.TipoCombustibles.AddRange(GetTipoCombustibles());
            context.Commit();
        }
        private static IEnumerable<TipoPersona> GetTipoPersonas()
        {
            return new List<TipoPersona>
            {
                new TipoPersona
                {
                    Descripcion = "Física",
                    Estado = true
                },
                new TipoPersona
                {
                    Descripcion = "Jurídica",
                    Estado = true
                }
            };
        }

        private static IEnumerable<Tanda> GetTandas()
        {
            return new List<Tanda>
            {
                new Tanda
                {
                    Descripcion = "Matutina",
                    Estado = true
                },
                new Tanda
                {
                    Descripcion = "Vespertina",
                    Estado = true
                },
                new Tanda
                {
                    Descripcion = "Nocturna",
                    Estado = true
                }
            };
        }

        private static IEnumerable<TipoVehiculo> GetTipoVehiculos()
        {
            return new List<TipoVehiculo>
            {
                new TipoVehiculo
                {
                    Descripcion = "Motocicleta",
                    Estado = true
                },
                new TipoVehiculo
                {
                    Descripcion = "Carro",
                    Estado = true
                },
                new TipoVehiculo
                {
                    Descripcion = "Jeep",
                    Estado = true
                },
                new TipoVehiculo
                {
                    Descripcion = "Camion",
                    Estado = true
                }
            };
        }

        private static IEnumerable<TipoCombustible> GetTipoCombustibles()
        {
            return new List<TipoCombustible>
            {
                new TipoCombustible
                {
                    Descripcion = "Gasolina",
                    Estado = true
                },
                new TipoCombustible
                {
                    Descripcion = "Gas Oil",
                    Estado = true
                },
                new TipoCombustible
                {
                    Descripcion = "Gas Natural",
                    Estado = true
                }
            };
        }
    }
}
