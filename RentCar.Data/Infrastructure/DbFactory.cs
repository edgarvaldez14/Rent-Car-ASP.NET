﻿namespace RentCar.Data.Infrastructure
{
    public class DbFactory : Disposable, IDbFactory
    {
        private RentCarEntities _dbContext;
        public RentCarEntities Init()
        {
            return _dbContext ?? (_dbContext = new RentCarEntities());
        }

        protected override void DisposeCore()
        {
            _dbContext?.Dispose();
        }
    }
}
