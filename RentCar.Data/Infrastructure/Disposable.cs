﻿using System;

namespace RentCar.Data.Infrastructure
{
    public class Disposable : IDisposable
    {
        private bool _isDisposed;

        protected virtual void DisposeCore()
        {

        }

        protected virtual void Dispose(bool disposing)
        {
            if (!_isDisposed && disposing)
            {
                DisposeCore();
            }

            _isDisposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        ~Disposable()
        {
            Dispose(false);
        }
    }
}
