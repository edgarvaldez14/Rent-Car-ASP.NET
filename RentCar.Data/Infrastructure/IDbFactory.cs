﻿using System;

namespace RentCar.Data.Infrastructure
{
    public interface IDbFactory : IDisposable
    {
        RentCarEntities Init();
    }
}
