﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using RentCar.Model;

namespace RentCar.Data.Infrastructure
{
    public abstract class RepositoryBase<T> where T : AbstractEntity
    {
        private RentCarEntities _dbContext;
        private readonly IDbSet<T> _dbSet;

        protected IDbFactory DbFactory { get; }
        protected RentCarEntities DbContext => _dbContext ?? (_dbContext = DbFactory.Init());

        protected RepositoryBase(IDbFactory dbFactory)
        {
            DbFactory = dbFactory;
            _dbSet = DbContext.Set<T>();
        }

        public virtual void Add(T entity)
        {
            _dbSet.Add(entity);
        }

        public virtual void Update(T entity)
        {
            _dbSet.Attach(entity);
            _dbContext.Entry(entity).State = EntityState.Modified;
        }

        public virtual void Delete(T entity)
        {
            _dbSet.Attach(entity);
            entity.Estado = false;
            _dbContext.Entry(entity).State = EntityState.Modified;
        }

        public virtual void Delete(Expression<Func<T, bool>> where)
        {
            var objects = _dbSet.Where(where).AsEnumerable();
            foreach (var obj in objects)
            {
                _dbSet.Attach(obj);
                obj.Estado = false;
                _dbContext.Entry(obj).State = EntityState.Modified;
            }
        }

        public virtual T GetById(int id, bool includeInactive = false)
        {
            var encontrado = _dbSet.Find(id);
            if (encontrado.Estado)
            {
                return encontrado;
            }
            return includeInactive ? encontrado : null;
        }

        public virtual IEnumerable<T> GetAll(bool includeInactive = false)
        {
            return includeInactive ? _dbSet.ToList() : _dbSet.Where(m => m.Estado).ToList();
        }
        
        public virtual IEnumerable<T> GetMany(Expression<Func<T, bool>> where)
        {
            return _dbSet.Where(where).ToList();
        }

        public T Get(Expression<Func<T, bool>> where)
        {
            return _dbSet.Where(where).FirstOrDefault();
        }
    }
}
