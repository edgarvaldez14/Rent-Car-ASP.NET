﻿namespace RentCar.Data.Infrastructure
{
    public interface IUnitOfWork
    {
        void Commit();
    }
}
